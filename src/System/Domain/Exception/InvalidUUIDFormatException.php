<?php

declare (strict_types = 1);

namespace System\Domain\Exception;

use Doctrine\DBAL\Exception\InvalidArgumentException;

class InvalidUUIDFormatException extends InvalidArgumentException
{
}