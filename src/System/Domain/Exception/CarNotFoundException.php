<?php

declare (strict_types = 1);

namespace System\Domain\Exception;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CarNotFoundException extends NotFoundHttpException
{
}