<?php

namespace System\Domain\Car;

interface Cars
{
    public function add(Car $car) : void;

    public function getByNumber(Number $number) : Car;
}