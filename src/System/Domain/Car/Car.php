<?php

declare (strict_types = 1);

namespace System\Domain\Car;

/**
 * @author Jakub Skoczeń <kodesko.com>
 */
final class Car
{
    private $id;

    private $registration;

    private $mark;

    private $model;

    private $year;

    private $color;

    public function __construct(string $registration, string $mark, string $model, int $year, string $color)
    {
        $this->registration = $registration;
        $this->mark = $mark;
        $this->model = $model;
        $this->year = $year;
        $this->color = $color;
    }
}