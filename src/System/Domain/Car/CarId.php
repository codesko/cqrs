<?php

declare (strict_types = 1);

namespace System\Domain\Car;

use System\Domain\Identity\UUID;
use Ramsey\Uuid\Uuid as BaseUUID;

/**
 * Class CarId
 * @package System\Domain\Car
 *
 * @author Jakub Skoczeń <kodesko.com>
 */
final class CarId extends UUID
{
    /**
     * @return CarId
     */
    public static function generate() : CarId
    {
        return new self((string) BaseUUID::uuid4());
    }
}