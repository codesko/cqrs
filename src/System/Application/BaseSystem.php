<?php

namespace System\Application;

use System\Application\Command\Command;
use System\Application\Query\Query;

/**
 * @author Jakub Skoczeń <kodesko.com>
 */
interface BaseSystem
{
    public function handle(Command $command) : void;

    public function query(string $queryClass);
}