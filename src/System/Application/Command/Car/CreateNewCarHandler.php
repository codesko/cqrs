<?php

declare (strict_types = 1);

namespace System\Application\Command\Car;

use System\Domain\Car\Car;
use System\Infrastructure\Doctrine\ORM\DoctrineCars;

/**
 * @author Jakub Skoczeń <kodesko.com>
 */
final class CreateNewCarHandler
{
    private $cars;

    public function __construct(DoctrineCars $cars)
    {
        $this->cars = $cars;
    }

    public function handle(CreateNewCarCommand $command) : void
    {
        $user = new Car(
            $command->registration(),
            $command->mark(),
            $command->model(),
            $command->year(),
            $command->color()
        );

        $this->cars->add($user);
    }
}