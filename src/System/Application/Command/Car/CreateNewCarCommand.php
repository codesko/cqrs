<?php

declare (strict_types = 1);

namespace System\Application\Command\Car;

use System\Application\Command\Command;

/**
 * @author Jakub Skoczeń <kodesko.com>
 */
final class CreateNewCarCommand implements Command
{
    private $registration;

    private $color;

    private $model;

    private $mark;

    private $year;

    public function __construct(string $registration, string $mark, string $model, int $year, string $color)
    {
        $this->registration = $registration;
        $this->mark  = $mark;
        $this->model = $model;
        $this->year  = $year;
        $this->color = $color;
    }

    public function registration() : string
    {
        return $this->registration;
    }

    public function color() : string
    {
        return $this->color;
    }

    public function model() : string
    {
        return $this->model;
    }

    public function mark() : string
    {
        return $this->mark;
    }

    public function year() : int
    {
        return $this->year;
    }
}