<?php

declare (strict_types=1);

namespace System\Application;

use Symfony\Component\DependencyInjection\ContainerInterface;
use System\Application\Command;
use System\Application\Query;
use System\Infrastructure\CommandBus\Adapter\Tactician as CommandBus;

/**
 * @author Jakub Skoczeń <kodesko.com>
 */
final class System implements BaseSystem
{
    private $commandBus;

    private $container;

    public function __construct(CommandBus $commandBus, ContainerInterface $container)
    {
        $this->container = $container;
        $this->commandBus = $commandBus;
    }

    public function handle(Command\Command $command) : void
    {
        try {
            $this->commandBus->handle($command);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function query(string $queryClass)
    {
        return $this->container->get($queryClass);
    }
}