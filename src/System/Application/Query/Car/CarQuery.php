<?php

declare (strict_types = 1);

namespace System\Application\Query\Car;

interface CarQuery
{
    public function getByRegistration(string $registration) : CarView;
}