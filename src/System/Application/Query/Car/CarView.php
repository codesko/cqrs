<?php

declare (strict_types = 1);

namespace System\Application\Query\Car;

/**
 * @author Jakub Skoczeń <kodesko.com>
 */
final class CarView
{
    public $id;

    private $registration;

    private $color;

    private $model;

    private $mark;

    private $year;

    public function __construct(string $id, string $registration, string $mark, string $model, int $year, string $color)
    {
        $this->id = $id;
        $this->registration = $registration;
        $this->mark  = $mark;
        $this->model = $model;
        $this->year  = $year;
        $this->color = $color;
    }

    public function registration() : string
    {
        return $this->registration;
    }

    public function color() : string
    {
        return $this->color;
    }

    public function model() : string
    {
        return $this->model;
    }

    public function mark() : string
    {
        return $this->mark;
    }

    public function year() : int
    {
        return $this->year;
    }
}