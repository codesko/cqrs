<?php

namespace System\Infrastructure\CommandBus\Adapter;

use League\Tactician\CommandBus;
use System\Application\Command\CommandBus as BaseCommandBus;

/**
 * @author Jakub Skoczeń <kodesko.com>
 */
final class Tactician implements BaseCommandBus
{
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @param $command
     */
    public function handle($command) : void
    {
        $this->commandBus->handle($command);
    }
}