<?php

namespace System\Infrastructure\Doctrine\DBAL;

use System\Application\Query\Car\CarQuery;

/**
 * @author Jakub Skoczeń <kodesko.com>
 */
final class DbalCarView implements CarQuery
{
    private $connection;

    public function __construct(\Doctrine\DBAL\Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getByRegistration(string $registration) : \System\Application\Query\Car\CarView
    {
        $result = $this->connection->fetchAssoc('
            SELECT * FROM car
            WHERE registration = :registration',
            [
                ':registration' => $registration,
            ]
        );

        if (!$result) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
        }

        return new \System\Application\Query\Car\CarView(
            $result['id'],
            $result['registration'],
            $result['mark'],
            $result['model'],
            $result['year'],
            $result['color']
        );
    }
}