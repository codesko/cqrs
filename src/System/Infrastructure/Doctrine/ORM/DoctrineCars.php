<?php

namespace System\Infrastructure\Doctrine\ORM;

use Doctrine\ORM\EntityManagerInterface;
use System\Domain\Car\Car;
use System\Domain\Car\Cars;
use System\Domain\Car\Number;

use System\Domain\Exception\CarNotFoundException;

final class DoctrineCars implements Cars
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function add(Car $car) : void
    {
        $this->entityManager->persist($car);
        $this->entityManager->flush();
    }

    public function getByNumber(Number $number) : Car
    {
        $car = $this->entityManager->getRepository(Car::class)->findOneBy([
            'number' => $number
        ]);

        if ($car === null) {
            throw new CarNotFoundException();
        }

        return $car;
    }
}