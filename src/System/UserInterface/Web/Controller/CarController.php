<?php

namespace System\UserInterface\Web\Controller;

use System\Application\Command\Car\CreateNewCarCommand;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use System\Application\Query\Car\CarQuery;
use System\Application\System;
use FOS\RestBundle\Controller\Annotations\Post;

/**
 * @author Jakub Skoczeń <kodesko.com>
 */
class CarController
{
    private $system;

    public function __construct(System $system)
    {
        $this->system = $system;
    }

    /**
     * @Post("/car")
     */
    public function createAction(Request $request) : JsonResponse
    {
        $command = new CreateNewCarCommand(
            (string) $request->get("registration"),
            (string) $request->get("mark"),
            (string) $request->get("model"),
            (int) $request->get("year"),
            (string) $request->get("color")
        );

        $this->system->handle($command);

        return new JsonResponse([
            'car' => $this->system->query(CarQuery::class)->getByRegistration((string) $request->get("registration"))
        ]);
    }
}